﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class RiverDialogueHolder : DialogHolder {

	public GameObject buddy1;
	public GameObject buddy2;

	public Transform pos1;
	public Transform pos2;
	public Transform pos3;

	public string levelToLoad;

	public override void PlayCutscene()  {

		StartCoroutine (WalkingAway ());
	}

	IEnumerator WalkingAway() {

		cutscene = true;

		foreach(string s in dialogueLines) {

			Debug.Log ("Waiting " + Time.time);

			yield return new WaitUntil (() => Input.GetKeyDown (KeyCode.E));
			yield return new WaitForEndOfFrame ();
		}

		yield return MoveCharacter (buddy1, pos1.position, pos3.position);
		yield return MoveCharacter (buddy1, pos3.position, pos1.position);
		yield return MoveCharacter (buddy2, pos3.position, pos2.position);

		// yield return RotateCharacter (buddy1, rot1, rot2);

		yield return ShowingDialogue ("Pharaoh’s daughter: Take this child and nurse it for me, and I will pay your wages.");	

		cutscene = false;

		SceneManager.LoadScene (levelToLoad);
	}
}
