﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class MidianEncounterHolder : DialogHolder {

	public BoxCollider2D box;

	public GameObject moses;
	public GameObject flock;
	public GameObject sheps;
//	public GameObject buddy2;
//
//	public Transform pos1;
//	public Transform pos2;
//	public Transform pos3;

	public Transform shepRight;
	public Transform shepMiddle;
	public Transform shepLeft;

	public Transform flockMiddle;

	public Transform mosesTalk;

	public Transform rot1;
	public Transform rot2;

	public string levelToLoad;

	public override void PlayCutscene()  {

		StartCoroutine (WalkingAway ());
	}

	IEnumerator WalkingAway() {

		cutscene = true;

		box.isTrigger = true;

		moses.transform.localScale = new Vector3 (-1f, 1f, 1f);

		yield return RotateCharacter (moses, rot1, rot2);

		yield return new WaitForSeconds (1);

		yield return MoveCharacter (flock, flock.transform.position, flockMiddle.position);

		yield return new WaitForSeconds (1);

		sheps.transform.localScale = new Vector3 (-1f, 1f, 1f);

		yield return MoveCharacter (sheps, shepRight.position, shepMiddle.position);
		 
		sheps.transform.localScale = new Vector3 (1f, 1f, 1f);

		yield return RotateCharacter (moses, rot2, rot1);

		yield return MoveCharacter (moses, moses.transform.position, mosesTalk.position);

		yield return new WaitForSeconds (1);

		yield return MoveCharacter (sheps, shepMiddle.position, shepLeft.position);	

		yield return ShowingDialogue ("Seven Daughters: Thank you for helping us.");	

//		yield return MoveCharacter (buddy1, pos1.position, pos3.position);
//		yield return MoveCharacter (buddy1, pos3.position, pos1.position);
//		yield return MoveCharacter (buddy2, pos3.position, pos2.position);

		// yield return RotateCharacter (buddy1, rot1, rot2);

		cutscene = false;

		SceneManager.LoadScene (levelToLoad);
	}
}