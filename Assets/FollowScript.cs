﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowScript : MonoBehaviour {

	public Transform following;
	public float followDistance;

	void Update () {

		transform.position = Vector3.Lerp (transform.position, following.position + new Vector3(followDistance, 0.0f, 0.0f), 0.05f);
	}
}
