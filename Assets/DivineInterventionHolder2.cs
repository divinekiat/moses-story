﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DivineInterventionHolder2 : DialogHolder {

	public Sprite newMoses;
	public SpriteRenderer mosesSR;

	public Sprite newSnake;
	public SpriteRenderer snakeSR;

	public GameObject Moses;
	public GameObject buddy2;

	public Transform rot1;
	public Transform rot2;

	public string levelToLoad;

	public override void PlayCutscene()  {

		StartCoroutine (WalkingAway ());
	}

	IEnumerator WalkingAway() {

		cutscene = true;

		foreach(string s in dialogueLines) {

			Debug.Log ("Waiting " + Time.time);

			yield return new WaitUntil (() => Input.GetKeyDown (KeyCode.E));
			yield return new WaitForEndOfFrame ();
		}

		yield return RotateCharacter (Moses, rot1, rot2);

		Sprite mosesOrig = mosesSR.sprite;

		mosesSR.sprite = newMoses;

		GameObject.FindGameObjectWithTag ("Rod").transform.position = new Vector3 (74.608f, 3.105f, 0.0f);

		yield return RotateCharacter (Moses, rot2, rot1);

		snakeSR.sprite = newSnake;

		yield return ShowingDialogue ("God: Put out your hand and grasp it by the tail.");	

		yield return RotateCharacter (Moses, rot1, rot2);

		GameObject.FindGameObjectWithTag ("Rod").transform.position = new Vector3 (74.608f, 100.105f, 0.0f);

		yield return RotateCharacter (Moses, rot2, rot1);

		mosesSR.sprite = mosesOrig;

		yield return ShowingDialogue ("God: Then they will believe you! Put your hand to your bosom");

		yield return ShowingDialogue ("***Moses did then it was covered in snowy scales. Then when he put it down, it went back to normal.***");

		yield return ShowingDialogue ("God: If they don't believe the first sign, they will believe the second sign.");	

		yield return ShowingDialogue ("God: And if they still are not convinced, take some water from the Nile ...");	

		yield return ShowingDialogue ("God: And pour it on the dry ground and it will turn to blood.");	

		yield return ShowingDialogue ("Moses: Please, o my Lord, I have never been good with words.");	

		yield return ShowingDialogue ("Moses: I am slow of speech and slow of tongue.");	

		yield return ShowingDialogue ("God: Who gives humans speech? Who makes them dumb or deaf, seeing or blind? Is it not I?");	

		yield return ShowingDialogue ("God: Now go, and I will be with you as you speak and will instruct you what to say.");

		yield return ShowingDialogue ("Moses: Please, make someone else Your agent.");	

		yield return ShowingDialogue ("***God became angry with Moses***");	

		yield return ShowingDialogue ("God: There is your brother Aaron the Levite. He, I know, speaks readily.");	

		yield return ShowingDialogue ("God: Even now, he is setting out to meet you, and he will be happy to see you.");

		yield return ShowingDialogue ("God: You shall speak to him and put the words in his mouth--");

		yield return ShowingDialogue ("God: I will be with you and with him as you speak, and tell both of you what to do--");

		yield return ShowingDialogue ("God: and he shall speak for you to the people.");

		yield return ShowingDialogue ("God: Take with you this rod, with which you shall perform the signs.");


		//		yield return MoveCharacter (buddy1, pos1.position, pos3.position);
		//		yield return MoveCharacter (buddy1, pos3.position, pos1.position);
		//		yield return MoveCharacter (buddy2, pos3.position, pos2.position);

		// yield return RotateCharacter (buddy1, rot1, rot2);

		//		yield return ShowingDialogue ("Pharaoh’s daughter: Take this child and nurse it for me, and I will pay your wages.");	

		cutscene = false;

		// SceneManager.LoadScene (levelToLoad);
	}
}



