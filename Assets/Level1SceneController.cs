﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level1SceneController : MonoBehaviour {
	
	public ChapterIntroScript cis;

	public GameObject buddy1;
	public GameObject buddy2;
	public GameObject buddy3;

	public string nextScene;

	void Start () {

		cis.ShowIntro ("Exodus 1");

		StartCoroutine (StartAnimating (buddy1));
		StartCoroutine (StartAnimating (buddy2));
		StartCoroutine (StartAnimating (buddy3));
		StartCoroutine (StartScene ());
	}

	IEnumerator StartAnimating(GameObject g) {
	
		yield return new WaitForSeconds (Random.Range (1, 3));

		while (true) {
		
			yield return MoveCharacter (g, g.transform.position, (g.transform.position + Vector3.right * 2));

			yield return new WaitForSeconds (1);

			yield return MoveCharacter (g, g.transform.position, (g.transform.position + Vector3.left * 2));

			yield return new WaitForSeconds (1);
		}
	}

	IEnumerator StartScene() {

		yield return new WaitForSeconds (4);

		cis.Fade ();

		yield return new WaitForSeconds (1);

		cis.ShowIntro ("For a time after Joseph's death, the Israelites were fertile and prolific. They multiplied and the land was filled with them.");

		yield return new WaitForSeconds (10);

		SceneManager.LoadScene (nextScene);
	}

	public IEnumerator MoveCharacter(GameObject g, Vector3 pos1, Vector3 pos2, float interval = 1) {

		float timeElapsed = 0;

		if (pos2.x > pos1.x)
			g.transform.localScale = new Vector3 (1, 1, 1);
		else 
			g.transform.localScale = new Vector3 (-1, 1, 1);

		while (timeElapsed < interval) {

			g.transform.position = Vector3.Lerp (pos1, pos2, timeElapsed / interval);

			timeElapsed += Time.deltaTime;
			yield return null;
		}

		g.transform.position = pos2;

		yield return null;
	}
}
