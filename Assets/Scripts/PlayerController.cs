﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public float moveSpeed;
	private float moveVelocity;
	public float jumpHeight;

	public Transform groundCheck;
	public float groundCheckRadius;
	public LayerMask whatIsGround;
	private bool grounded;

	private bool doubleJumped;

	private Animator anim;

	// public Transform firePoint;
	// public GameObject bullet;

	// public float shotDelay;
	// private float shotDelayCounter;

	public float knockback;
	public float knockbackLength;
	public float knockbackCount;
	public bool knockfromRight;

	private Rigidbody2D myrigidbody2D;

	public bool canMove;

	void Start () {

		anim = GetComponent<Animator> ();

		myrigidbody2D = GetComponent<Rigidbody2D> ();

		canMove = true;
	}

	void FixedUpdate () {

		grounded = Physics2D.OverlapCircle (groundCheck.position, groundCheckRadius, whatIsGround);
	}

	void Update () {
		
		if (!canMove || DialogHolder.cutscene || ChapterIntroScript.chapterIntroShowing) {

//			myrigidbody2D.velocity = Vector2.zero;
			myrigidbody2D.velocity = Vector2.up * myrigidbody2D.velocity.y;

			return;
		}

		if (grounded)
			doubleJumped = false;

		// anim.SetBool ("Grounded", grounded);

		if (Input.GetKeyDown (KeyCode.Space) && grounded) {

			Jump ();
		}

		if (Input.GetKeyDown (KeyCode.Space) && !doubleJumped && !grounded) {

			Jump ();
			doubleJumped = true;
		}

		moveVelocity = 0f;

		if (Input.GetKey (KeyCode.D)) {

			// GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			moveVelocity = moveSpeed;
		}

		if (Input.GetKey (KeyCode.A)) {

			// GetComponent<Rigidbody2D> ().velocity = new Vector2 (-moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
			moveVelocity = -moveSpeed;
		}

		if (knockbackCount <= 0) {

			myrigidbody2D.velocity = new Vector2 (moveVelocity, myrigidbody2D.velocity.y);
		} else {

			if (knockfromRight) {

				myrigidbody2D.velocity = new Vector2 (-knockback, knockback);
			} 

			if (!knockfromRight) {

				myrigidbody2D.velocity = new Vector2 (knockback, knockback);
			}

			knockbackCount -= Time.deltaTime;
		}

		if(anim != null)
			anim.SetFloat ("Speed", Mathf.Abs(myrigidbody2D.velocity.x));

		if (myrigidbody2D.velocity.x > 0) 
			transform.localScale = new Vector3 (1f, 1f, 1f);
		else if (myrigidbody2D.velocity.x < 0)
			transform.localScale = new Vector3 (-1f, 1f, 1f);

//		if (Input.GetKeyDown (KeyCode.Return)) {
//
//			Instantiate (bullet, firePoint.position, firePoint.rotation);
//			shotDelayCounter = shotDelay;
//		}
//
//		if (Input.GetKey (KeyCode.Return)) {
//
//			shotDelayCounter -= Time.deltaTime;
//
//			if (shotDelayCounter <= 0) {
//
//				shotDelayCounter = shotDelay;
//				Instantiate (bullet, firePoint.position, firePoint.rotation);
//			}
//		}
	}

	public void Jump () {
		
		myrigidbody2D.velocity = new Vector2 (GetComponent<Rigidbody2D> ().velocity.x, jumpHeight);
	}
}
