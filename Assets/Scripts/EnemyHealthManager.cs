﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHealthManager : MonoBehaviour {

	public int enemyHealth;

	public GameObject deathEffect;

	public int pointsOnDeath;

	public bool withParticle;

	public float lifetime;

	void Start () {
		
	}

	void Update () {

		if (enemyHealth <= 0) {
			if (withParticle)
				Instantiate (deathEffect, transform.position, transform.rotation);
			else {
				GetComponent<CircleCollider2D>() .enabled = false;
				GetComponent<EnemyPatrol>() .enabled = false;
			}

			lifetime -= Time.deltaTime;

			if (lifetime < 0) {
				ScoreManager.AddPoints (pointsOnDeath);
				Destroy (gameObject);
			}
		}
		
	}

	public void giveDamage(int damageToGive) {

		enemyHealth -= damageToGive;
		GetComponent<AudioSource> ().Play ();
	}
}
