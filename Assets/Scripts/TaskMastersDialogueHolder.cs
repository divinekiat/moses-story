﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TaskMastersDialogueHolder : DialogHolder {

	public override void PlayCutscene()  {
	
		StartCoroutine (WalkingAway ());
	}

	IEnumerator WalkingAway() {

		cutscene = true;

		foreach(string s in dialogueLines) {

			Debug.Log ("Waiting " + Time.time);

			yield return new WaitUntil (() => Input.GetKeyDown (KeyCode.E));
			yield return new WaitForEndOfFrame ();
		}

		yield return new WaitForSeconds (3);

		cutscene = false;
	}

	public override void DoThing() {
		
		foreach (GameObject wa in GameObject.FindGameObjectsWithTag("Talk_TaskMaster"))
			wa.GetComponent<WalkAway>().isMoving = true;
	}
}
