﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DialogHolder : MonoBehaviour {

	public static bool cutscene;

	public string dialogue;
	public DialogueManager dMan;

	public string[] dialogueLines;

	public bool triggered;

	void Start () {

		dMan = FindObjectOfType<DialogueManager> ();

		triggered = false;
	}

	void OnTriggerEnter2D (Collider2D other) {

		if (other.gameObject.name == "Player" && !triggered) {

			Debug.Log ("Doing stuff!");

			triggered = true;
			// dMan.ShowBox (dialogue, gameObject);

			PlayCutscene ();

			if(other.GetComponent<Animator>() != null)
				other.GetComponent<Animator> ().SetFloat ("Speed", 0);

			if(dMan == null)
				dMan = FindObjectOfType<DialogueManager> ();

			if (!dMan.dialogActive) {

				Debug.Log ("Showing dialogue: " + name);

				dMan.dialogLines = dialogueLines;
				dMan.currentLine = 0;
				dMan.ShowDialogue (this);
			}
				
		}
	}

	public virtual void PlayCutscene() {
		
	}

	public virtual void DoThing() {
		
	}

	public IEnumerator MoveCharacter(GameObject g, Vector3 pos1, Vector3 pos2, float interval = 2) {

		float timeElapsed = 0;

		if (pos2.x > pos1.x)
			g.transform.localScale = new Vector3 (1, 1, 1);
		else 
			g.transform.localScale = new Vector3 (-1, 1, 1);

		while (timeElapsed < interval) {

			g.transform.position = Vector3.Lerp (pos1, pos2, timeElapsed / interval);

			timeElapsed += Time.deltaTime;
			yield return null;
		}

		g.transform.position = pos2;

		yield return null;
	}

	public IEnumerator RotateCharacter(GameObject g, Transform rot1, Transform rot2, float interval = 1) {

		float timeElapsed = 0;

		while (timeElapsed < interval) {

			g.transform.rotation = Quaternion.Lerp (rot1.rotation, rot2.rotation, timeElapsed / interval);

			timeElapsed += Time.deltaTime;
			yield return null;
		}

		yield return null;
	}

	public IEnumerator ShowingDialogue(string message) {

		dMan.ShowBox (message, this.gameObject);

		yield return new WaitForEndOfFrame ();
		yield return new WaitUntil (() => Input.GetKeyDown (KeyCode.E));

//		To use:
//		yield return ShowingDialogue (message);

//		yield return new WaitForSeconds (2);
	}
}

