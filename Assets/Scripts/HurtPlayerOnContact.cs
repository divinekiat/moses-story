﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtPlayerOnContact : MonoBehaviour {

	public int damageToGive;

	void Start () {
		
	}

	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		
		if (other.name == "Player") {

			HealthManager.HurtPlayer (damageToGive);
			other.GetComponent<AudioSource> ().Play ();

			var player = other.GetComponent<PlayerController> ();
			player.knockbackCount = player.knockbackLength;

			if (other.transform.position.x < transform.position.x)
				player.knockfromRight = true;
			else
				player.knockfromRight = false;
		}
	}
}
