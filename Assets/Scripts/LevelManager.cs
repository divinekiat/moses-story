﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour {

	public GameObject currentCheckpoint;

	private PlayerController player;

	public GameObject deathParticle;
	public GameObject respawnParticle;

	public int pointPenaltyOnDeath;

	public float respawnDelay;

	private CameraController mainCam;

	private float gravityStore;

	public HealthManager healthManager;

	public string chapterName;

	public float fadeTime;

	public ChapterIntroScript cis;

	void Start () {

		player = FindObjectOfType<PlayerController> ();
		mainCam = FindObjectOfType<CameraController> ();

		healthManager = FindObjectOfType<HealthManager> ();

		if (ChapterIntroScript.Instance != null) {
		
			ChapterIntroScript.Instance.ShowIntro (chapterName, fadeTime);
			ChapterIntroScript.Instance.Fade ();
		} else {
		
			cis.ShowIntro (chapterName, fadeTime);
			cis.Fade ();
		}
	}

	void Update () {

	}

	public void RespawnPlayer() {

		StartCoroutine ("RespawnPlayerCo");
	}

	public IEnumerator RespawnPlayerCo () {
		
		Instantiate (deathParticle, player.transform.position, player.transform.rotation);
		player.enabled = false;
		player.GetComponent<Renderer> ().enabled = false;
		mainCam.isFollowing = false;
		gravityStore = player.GetComponent<Rigidbody2D> ().gravityScale;
		player.GetComponent<Rigidbody2D> ().gravityScale = 0f;
		player.GetComponent<Rigidbody2D> ().velocity = Vector2.zero; 
		ScoreManager.AddPoints (-pointPenaltyOnDeath);
		Debug.Log ("Player Respawn");
		yield return new WaitForSeconds (respawnDelay);
		player.GetComponent<Rigidbody2D> ().gravityScale = gravityStore;
		player.transform.position = currentCheckpoint.transform.position;
		player.knockbackCount = 0;
		player.enabled = true;
		player.GetComponent<Renderer> ().enabled = true;
		healthManager.FullHealth ();
		healthManager.isDead = false;
		mainCam.isFollowing = true;
		Instantiate(respawnParticle, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
	}
}
