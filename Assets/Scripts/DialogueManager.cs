﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogueManager : MonoBehaviour {

	public GameObject dBox;
	public Text dText;

	public bool dialogActive;

	bool showingbox;

	public string[] dialogLines;
	public int currentLine;

	private PlayerController thePlayer;
	private DialogHolder go;

	public GameObject dialogue;

	void Start () {

		thePlayer = FindObjectOfType<PlayerController> ();
		showingbox = false;

		dBox.SetActive (false);
	}

	void Update () {

		if (dialogActive && Input.GetKeyDown(KeyCode.E) && !showingbox) {

			// dBox.SetActive (false);
			// dialogActive = false;

			showingbox = false;

			currentLine++;
		}

		if (showingbox && Input.GetKeyDown (KeyCode.E)) {
		
			dBox.SetActive (false);
			dialogActive = false;
			showingbox = false;
		}

		if (currentLine >= dialogLines.Length && !showingbox) {
			
			dBox.SetActive (false);
			dialogActive = false;

			currentLine = 0;
			thePlayer.canMove = true;

			if (go != null) {
				go.DoThing ();
			}
		}

		if (currentLine < dialogLines.Length && !showingbox)
			dText.text = dialogLines[currentLine];
	}

	public void ShowBox (string dialogue, GameObject go) {

		Debug.Log ("Show box: " + dialogue);

		showingbox = true;

		dialogActive = true;

		dText.text = dialogue;
		dBox.SetActive (true);
	}

	public void ShowDialogue (DialogHolder go) {

		Debug.Log ("Show DIALOGUE");

		dialogActive = true;
		dBox.SetActive (true);
		thePlayer.canMove = false;
		this.go = go;
	}


}
