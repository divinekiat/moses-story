﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletController : MonoBehaviour {

	public float speed;

	public PlayerController player;

	public GameObject enemyDeathEffect;

	public GameObject impactEffect;

	public int pointForKill;

	public float rotationSpeed;

	public int damageToGive;

	void Start () {
		player = FindObjectOfType<PlayerController> ();

		if (player.transform.localScale.x < 0) {
			speed = -speed;
			rotationSpeed = -rotationSpeed;
		}
	}

	void Update () {

		GetComponent<Rigidbody2D> ().velocity = new Vector2 (speed, GetComponent<Rigidbody2D> ().velocity.y);

		GetComponent<Rigidbody2D> ().angularVelocity = rotationSpeed;
	}

	void OnTriggerEnter2D(Collider2D other) {

		if (other.tag == "Enemy") {

//			Instantiate (enemyDeathEffect, other.transform.position, other.transform.rotation);
//			Destroy (other.gameObject);
//			ScoreManager.AddPoints (pointForKill);

			other.GetComponent<EnemyHealthManager> ().giveDamage (damageToGive);
		}

		Instantiate (impactEffect, transform.position, transform.rotation);
		Destroy (gameObject);
	}

	void OnCollisionEnter2D (Collision2D other) {

		if (other.gameObject.layer == 8) {

			Instantiate (impactEffect, transform.position, transform.rotation);
			Destroy (gameObject);
		}
	}
}
