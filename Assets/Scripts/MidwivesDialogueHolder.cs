﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MidwivesDialogueHolder : DialogHolder {

	public override void PlayCutscene()  {

		StartCoroutine (WalkingAway ());
	}

	IEnumerator WalkingAway() {

		cutscene = true;

		foreach(string s in dialogueLines) {

			Debug.Log ("Waiting " + Time.time);

			yield return new WaitUntil (() => Input.GetKeyDown (KeyCode.E));
			yield return new WaitForEndOfFrame ();
		}

		yield return new WaitForSeconds (3);

		cutscene = false;
	}

	public override void DoThing() {

		Debug.Log ("Doing thing");

		foreach (GameObject wa in GameObject.FindGameObjectsWithTag("Talk_Midwives")) {

			wa.GetComponent<WalkAway>().isMoving = true;
			Debug.Log ("Bruh");
		}
	}
}
