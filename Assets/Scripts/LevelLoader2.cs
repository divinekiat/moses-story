﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLoader2 : MonoBehaviour {

	private bool playerInZone;

	public bool doneTalking;

	public string levelToLoad;

	void Start () {

		playerInZone = false;
		doneTalking = false;
	}

	void Update () {

		if (playerInZone) {

			SceneManager.LoadScene (levelToLoad);
		}
	}

	void OnTriggerEnter2D(Collider2D other) {

		if (other.name == "Player" && doneTalking) {

			playerInZone = true;
		}
	}

	void OnTriggerExit2D(Collider2D other) {

		if (other.name == "Player") {

			playerInZone = false;
		}
	}
}
