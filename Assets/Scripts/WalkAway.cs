﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkAway : MonoBehaviour {

	public float moveSpeed;
	public bool isMoving;
	private Animator anim;

	void Start () {
		
		isMoving = false;
		anim = GetComponent<Animator> ();
	}

	void Update () {
		
		if (isMoving) {
			
			transform.localScale = new Vector3 (1f, 1f, 1f);
			GetComponent<Rigidbody2D> ().velocity = new Vector2 (moveSpeed, GetComponent<Rigidbody2D> ().velocity.y);
		}

		anim.SetFloat ("Speed", Mathf.Abs(GetComponent<Rigidbody2D> ().velocity.x));
	}

	void OnBecameInvisible() {

		gameObject.SetActive (false);
	}
}
