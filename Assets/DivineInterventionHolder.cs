﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class DivineInterventionHolder : DialogHolder {

	public GameObject buddy1;
	public GameObject buddy2;

	public Transform pos1;
	public Transform pos2;
	public Transform pos3;

	public string levelToLoad;

	public override void PlayCutscene()  {

		StartCoroutine (WalkingAway ());
	}

	IEnumerator WalkingAway() {

		cutscene = true;

		foreach(string s in dialogueLines) {

			Debug.Log ("Waiting " + Time.time);

			yield return new WaitUntil (() => Input.GetKeyDown (KeyCode.E));
			yield return new WaitForEndOfFrame ();
		}

		FindObjectOfType<PlayerController>().transform.localScale = new Vector3 (-1f, 1f, 1f);

		yield return ShowingDialogue ("God: I heard the people in Egypty are suffering.");	
		yield return ShowingDialogue ("God: I have come to rescue them ...");	
		yield return ShowingDialogue ("God: And bring them to a good land, flowing with milk and honey!");	
		yield return ShowingDialogue ("God: I will send you to Pharaoh, and you shall free My people, the Israelites from Egypt. ");	
		yield return ShowingDialogue ("Moses: Who am I that I should go to Pharaoh and free the Israelites from Egypt?");	
		yield return ShowingDialogue ("God: I will be with you; that shall be your sign that it was I who sent you.");	
		yield return ShowingDialogue ("God: And when you have freed the people from Egypt, you shall worship God at this mountain");	
		yield return ShowingDialogue ("Moses: When I come to the Israelites and say to them, 'The God of your ancestors has sent me to you,'");	
		yield return ShowingDialogue ("Moses: and they ask me, 'What is his name?'");
		yield return ShowingDialogue ("Moses: What shall I say to them?");
		yield return ShowingDialogue ("God: Ehyeh-Asher-Ehyeh ...");
		yield return ShowingDialogue ("God: thus you shall say to the Israelites, 'Ehyeh sent me to you'");
		yield return ShowingDialogue ("God: Thus, you shall speak to the Israelites,'YHWH, ...");
		yield return ShowingDialogue ("God: 'YHWH, the God of your ancestors--the God of Abraham, the God of Isaac, and the God of Jacob ...");
		yield return ShowingDialogue ("God: 'has sent me to you: ... ");
		yield return ShowingDialogue ("          'This shall be My name forever\n          This My appelation for all eternity'");
		yield return ShowingDialogue ("God: Assemble the elders of Israel and say God has appeared to you");
		yield return ShowingDialogue ("God: You will take them out of misery to a land flowing with milk and honey.");
		yield return ShowingDialogue ("God: I know that the king of Egypt wouldn't let Israelites go so easily ...");
		yield return ShowingDialogue ("God: I will smite Egypt with various wonders!");
		yield return ShowingDialogue ("God: I will make sure they will not leave empty-handed--");
		yield return ShowingDialogue ("God: I will make sure they will have silver, gold, and clothing which they would give their sons and daughters.");

//		yield return MoveCharacter (buddy1, pos1.position, pos3.position);
//		yield return MoveCharacter (buddy1, pos3.position, pos1.position);
//		yield return MoveCharacter (buddy2, pos3.position, pos2.position);

		// yield return RotateCharacter (buddy1, rot1, rot2);

//		yield return ShowingDialogue ("Pharaoh’s daughter: Take this child and nurse it for me, and I will pay your wages.");	

		cutscene = false;

		SceneManager.LoadScene (levelToLoad);
	}
}



