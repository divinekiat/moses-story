﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChapterIntroScript : MonoBehaviour {

	private static ChapterIntroScript instance;

	public static ChapterIntroScript Instance {

		get {

			return instance;
		}
	}

	public static bool chapterIntroShowing;

	public Text chapterName;
	public GameObject blackOverlay;

	public float fadeTime;

	void Start() {

		if (instance == null)
			instance = this;
		else
			Destroy (gameObject);

		DontDestroyOnLoad (gameObject);

		chapterIntroShowing = false;
	}

	public void ShowIntro(string name, float fadeTime = 1) {

		chapterName.text = name;
		this.fadeTime = fadeTime;
		StartCoroutine (ShowingIntro ());
	}

	public void Fade() {

		StartCoroutine (Fading ());
	}

	IEnumerator Fading() {

		chapterIntroShowing = true;

		blackOverlay.SetActive (true);

		float timeElapsed = 0;

		Color startColor = Color.clear;
		Color endColor = Color.black;

		timeElapsed = 0;

		while (timeElapsed <= fadeTime) {

			blackOverlay.GetComponent<Image> ().color = Color.Lerp (startColor, endColor, timeElapsed / 2);
			timeElapsed += Time.deltaTime;

			yield return null;
		}

		yield return new WaitForSeconds (1);

		timeElapsed = 0;

		endColor = Color.clear;
		startColor = Color.black;

		while (timeElapsed <= fadeTime) {

			blackOverlay.GetComponent<Image> ().color = Color.Lerp (startColor, endColor, timeElapsed / 2);
			timeElapsed += Time.deltaTime;

			yield return null;
		}


		chapterIntroShowing = false;
	}

	IEnumerator ShowingIntro() {

		Debug.Log ("Showing Intro");

		GameObject originalCanvas = GameObject.FindGameObjectWithTag ("OriginalCanvas");

		chapterIntroShowing = true;

		Debug.Log ("Set overlay active");

		blackOverlay.SetActive (true);
		chapterName.gameObject.SetActive (true);

		if(originalCanvas != null)
			originalCanvas.SetActive (false);

		chapterIntroShowing = true;

		float timeElasped = 0;
		Color startColor = Color.clear;
		Color endColor = Color.white;

		while(timeElasped < fadeTime) {

			chapterName.color = Color.Lerp (startColor, endColor, timeElasped / 1);

			timeElasped += Time.deltaTime;

			yield return null;
		}

		chapterName.color = endColor;

		yield return new WaitForSeconds (2);

		timeElasped = 0;
		startColor = Color.white;
		endColor = Color.clear;

		while(timeElasped < fadeTime) {

			chapterName.color = Color.Lerp (startColor, endColor, timeElasped / 1);

			timeElasped += Time.deltaTime;

			yield return null;
		}

		chapterName.color = endColor;

		if(originalCanvas != null)
			originalCanvas.SetActive (true);

		blackOverlay.SetActive (false);
		chapterName.gameObject.SetActive (false);

		chapterIntroShowing = false;
	}
}
