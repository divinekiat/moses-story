﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalIntro : MonoBehaviour {

	public string introMessage;

	void Start() {
		
		StartCoroutine (ShowNext ());
	}

	IEnumerator ShowNext() {

		yield return new WaitForSeconds (6);

		ChapterIntroScript.Instance.ShowIntro (introMessage);
		ChapterIntroScript.Instance.Fade ();
	}
}
