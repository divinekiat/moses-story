﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class RiverDialogueHolder2 : DialogHolder {

	public GameObject buddy1;

	public FollowScript fs;

	public Rigidbody2D rb;

	public Transform pos1;
	public Transform pos2;
	public Transform pos3;
	public Transform pos4;

	public string levelToLoad;

	public override void PlayCutscene()  {

		StartCoroutine (WalkingAway ());
	}

	void OnTriggerEnter2D (Collider2D other) {

		if (other.gameObject.name == "Player" && !triggered) {


			triggered = true;
			// dMan.ShowBox (dialogue, gameObject);

			PlayCutscene ();

			if(other.GetComponent<Animator>() != null)
				other.GetComponent<Animator> ().SetFloat ("Speed", 0);
		}
	}

	IEnumerator WalkingAway() {

		fs.enabled = false;
		rb.gravityScale = 0;

		cutscene = true;

		yield return MoveCharacter (buddy1, pos1.position, pos2.position, 0.5f);
		yield return MoveCharacter (buddy1, pos2.position, pos3.position, 0.5f);
		yield return MoveCharacter (buddy1, pos3.position, pos4.position, 0.5f);

		buddy1.transform.position = pos4.position;

		if (dMan == null)
			dMan = FindObjectOfType<DialogueManager> ();

		dMan.dialogLines = dialogueLines;
		dMan.currentLine = 0;
		dMan.ShowDialogue (this);

		yield return new WaitUntil (() => Input.GetKeyDown (KeyCode.E));
		yield return new WaitForEndOfFrame ();

		yield return new WaitForSeconds (1);

		cutscene = false;

		SceneManager.LoadScene (levelToLoad);
	}
}
